<?php


namespace app\botCore\entities\user;



use app\botCore\dto\user\RouteDto;
use app\botCore\entities\base\Entity;

/**
 * Class User
 * @package app\botCpre\entities\user
 *
 * @property int $id
 * @property string $username
 * @property string $firstName
 * @property string $lastName
 * @property string $languageCode
 * @property string $telegramId
 * @property string $isBot
 * @property RouteDto $route
 */
final class User extends Entity
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string|null
     */
    public ?string $username;

    /**
     * @var string
     */
    public string $firstName;

    /**
     * @var string|null
     */
    public ?string $lastName;

    /**
     * @var Route
     */
    public Route $route;

}