<?php


namespace app\botCore\entities\user;


use app\botCore\entities\base\Entity;

/**
 * Class Route
 * @package app\botCore\entities\user
 */
final class Route extends Entity
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $controller;

    /**
     * @var string
     */
    public string $action;

    /**
     * @var int
     */
    public int $userId;

}