<?php


namespace app\botCore\entities\update;


use app\botCore\dto\update\MessageDto;
use app\botCore\entities\base\Entity;

class Message extends Entity
{
    /**
     * Message constructor.
     * @param MessageDto $dto
     */
    public function __construct(MessageDto $dto)
    {
        parent::__construct($dto);
        $this->from = new User($dto->from);
        $this->chat = new Chat($dto->chat);
        $this->date = $dto->date;
        $this->text = $dto->text;
    }
    /**
     * @var int
     */
    public int $messageId;

    /**
     * @var ?User
     */
    public ?User $from;

    /**
     * @var ?Chat
     */
    public ?Chat $chat;

    /**
     * @var int
     */
    public int $date;

    /**
     * @var string
     */
    public string $text;

}