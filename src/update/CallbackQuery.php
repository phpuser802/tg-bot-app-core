<?php


namespace app\botCore\entities\update;



use app\botCore\dto\update\CallbackQueryDto;
use app\botCore\entities\base\Entity;

class CallbackQuery extends Entity
{
    /**
     * CallbackQuery constructor.
     * @param CallbackQueryDto $dto
     */
    public function __construct(CallbackQueryDto $dto)
    {
        parent::__construct($dto);
        $this->id = $dto->id;
        $this->from = new User($dto->from);
        $this->message = new Message($dto->message);
        $this->chatInstance = $dto->chatInstance;
        $this->data = $dto->data;
    }

    /**
     * @var string
     */
    public string $id;

    /**
     * @var User
     */
    public User $from;

    /**
     * @var Message
     */
    public Message $message;

    /**
     * @var string
     */
    public string $chatInstance;

    /**
     * @var string
     */
    public string $data;

}