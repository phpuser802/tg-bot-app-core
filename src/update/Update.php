<?php


namespace app\botCore\entities\update;



use app\botCore\dto\update\UpdateDto;
use app\botCore\entities\base\Entity;
use InvalidArgumentException;

/**
 * Class Request
 * @package entities\request
 *
 * @property-read int $updateId
 * @property-read Message $message
 * @property-read CallbackQuery $callbackQuery
 * @property-read Chat $chat
 * @property-read int $messageId
 * @property-read User $from
 */
class Update extends Entity
{
    /**
     * Update constructor.
     * @param UpdateDto $dto
     */
    public function __construct(UpdateDto $dto)
    {var_dump($dto->toArray());die;
        parent::__construct($dto);
        $this->updateId = $dto->updateId;
        $this->message = $dto->message ? new Message($dto->message) : null;
        $this->callbackQuery = $dto->callbackQuery ? new CallbackQuery($dto->callbackQuery) : null;
    }

    const TYPE_MESSAGE = 'message';
    const TYPE_CALLBACK_QUERY = 'callbackQuery';

    /**
     *
     * @var int
     */
    private int $updateId;

    /**
     * @var Message|null
     */
    private ?Message $message;

    /**
     * @var CallbackQuery|null
     */
    private ?CallbackQuery $callbackQuery;

    /**
     * @param int $updateId
     */
    public function setUpdateId(int $updateId): void
    {
        $this->updateId = $updateId;
    }

    /**
     * @return int
     */
    public function getUpdateId(): int
    {
        return $this->updateId;
    }

    /**
     * @param ?Message $message
     */
    public function setMessage(?Message $message): void
    {
        $this->message = $message;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param ?CallbackQuery $callbackQuery
     */
    public function setCallbackQuery(?CallbackQuery $callbackQuery): void
    {
        $this->callbackQuery = $callbackQuery;
    }

    /**
     * @return ?CallbackQuery
     */
    public function getCallbackQuery(): ?CallbackQuery
    {
        return $this->callbackQuery;
    }

    /**
     * @return Chat|null
     */
    public function getChat(): ?Chat
    {var_dump($this->callbackQuery);die;
        return $this->message->chat ?? $this->callbackQuery->message->chat;
    }

    /**
     * @return User|null
     */
    public function getFrom(): ?User
    {
        return $this->message->from ?? $this->callbackQuery->message->from;
    }

}