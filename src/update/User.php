<?php


namespace app\botCore\entities\update;


use app\botCore\entities\base\Entity;

/**
 * Class User
 * @package app\botCore\entities\update
 */
class User extends Entity
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var bool
     */
    public bool $isBot;

    /**
     * @var string|null
     */
    public ?string $firstName;

    /**
     * @var string|null
     */
    public ?string $lastName;

    /**
     * @var string
     */
    public string $username;

    /**
     * @var string
     */
    public string $languageCode;
}