<?php


namespace app\botCore\entities\update;


use app\botCore\entities\base\Entity;


/**
 * Class Chat
 * @package app\botCore\entities\update
 *
 * @property int $id;
 */
class Chat extends Entity
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $firstName;

    /**
     * @var string|null
     */
    public ?string $lastName;

    /**
     * @var string|null
     */
    public ?string $username;

    /**
     * @var string
     */
    public string $type;

}