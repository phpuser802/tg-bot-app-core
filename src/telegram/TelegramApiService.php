<?php

namespace Jeka\TgBotAppCore\telegram;


use Jeka\TgBotAppCore\traits\MagicGetSetTrait;

/**
 * Class ContentOutputService
 * @package app\services\telegram
 *
 * @property-write ConfigDto $config
 * @property-read TelegramApiInterface $telegramApi
 */
class TelegramApiService
{
    use MagicGetSetTrait;

    /**
     * @var TelegramApiInterface
     */
    private TelegramApiInterface $telegramApi;

    /**
     * TelegramApiService constructor.
     * @param TelegramApiInterface $telegramApi
     */
    public function __construct(TelegramApiInterface $telegramApi)
    {
        $this->telegramApi = $telegramApi;
    }

    /**
     * @param ConfigDto $dto
     */
    public function setConfig(ConfigDto $dto)
    {
        $this->telegramApi->config = $dto;
    }

    /**
     * @param PageDto $dto
     * @return array
     * @throws GuzzleException
     */
    public function render(PageDto $dto): array
    {
        $response = $this->telegramApi->sendMessage([
            'chat_id'      =>  $dto->chatId,
            'text'         =>  $dto->text,
            'parse_mode'   =>  $dto->parseMode,
            'reply_markup' =>  $dto->replyMarkup,
        ]);

        return json_decode($response, true);
    }
}