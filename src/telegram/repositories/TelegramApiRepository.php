<?php

namespace Jeka\TgBotAppCore\repositories;

use app\botCore\dto\config\ConfigDto;
use app\botCore\interfaces\api\TelegramApiInterface;
use app\botCore\traits\MagicGetSetTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class TelegramApiRepository
 *
 * @property-write $config;
 * @property-read Client $client
 */
class TelegramApiRepository implements TelegramApiInterface
{
    use MagicGetSetTrait;

    /**
     * @var string api Telegram url.
     */
    private string $apiUrl;

    /**
     * @var string access token.
     */
    private string $botToken;

    /**
     * @param ConfigDto $dto
     */
    public function setConfig(ConfigDto $dto): void
    {
        $this->apiUrl = $dto->apiUrl;
        $this->botToken = $dto->botToken;
    }

    /**
     * @return Client
     */
    public function getClient() : Client
    {
        return new Client(['base_uri' => $this->apiUrl]);
    }

    /**
     * @param string $url
     * @return string
     * @throws GuzzleException
     */
    public function setWebHook(string $url) : string
    {
        return $this->send('/setWebHook', ['url' => $url]);
    }

    /**
     * @param array $params
     * @return string
     * @throws GuzzleException
     */
    public function sendMessage(array $params) : string
    {
        return $this->send('/sendMessage', $params);
    }

    /**
     * @param $params
     * @return string
     * @throws GuzzleException
     */
    public function editMessageReplyMarkup($params) : string
    {
        return $this->send('/editMessageReplyMarkup', $params);
    }

    /**
     * @param $params
     * @return string
     * @throws GuzzleException
     */
    public function deleteMessage($params) : string
    {
        return $this->send('/deleteMessage', $params);
    }

    /**
     * @param $params
     * @return string
     * @throws GuzzleException
     */
    public function sendPhoto($params) : string
    {
        return $this->send('/sendPhoto', $params);
    }

    /**
     * @param string $method
     * @param array $params
     * @return string
     * @throws GuzzleException
     */
    private function send(string $method, array $params) : string
    {
        $uri = '/bot' . $this->botToken . $method;
        return $this->client->post($uri, ["form_params" => $params])->getBody()->getContents();
    }

}