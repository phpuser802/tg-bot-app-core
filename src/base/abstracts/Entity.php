<?php


namespace botCore\base\abstracts;


use botCore\base\traits\Constructor;
use botCore\base\traits\MagicGetSetTrait;

/**
 * Class BaseEntity
 * @package app\entities\base
 */
abstract class Entity
{
    use MagicGetSetTrait, Constructor;
}