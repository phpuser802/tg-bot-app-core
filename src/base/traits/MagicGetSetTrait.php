<?php


namespace botCore\base\traits;



use Exception;

/**
 * Trait MagicGetSetTrait
 * @package app\botCore\traits
 */
trait MagicGetSetTrait
{
    /**
     * Returns the value of a component property.
     *
     * @param string $name the property name
     * @return mixed the property value or the value of a behavior's property
     * @throws Exception if the property is not defined
     * @throws Exception if the property is write-only.
     * @see __set()
     */
    public function __get(string $name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            // read property, e.g. getName()
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new Exception('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Sets the value of a component property.
     *
     * @param string $name the property name or the event name
     * @param mixed $value the property value
     * @throws Exception if the property is not defined
     * @throws Exception if the property is read-only.
     * @see __get()
     */

    public function __set(string $name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            // set property
            $this->$setter($value);

            return;
        }

        if (method_exists($this, 'get' . $name)) {
            throw new Exception('Setting read-only property: ' . get_class($this) . '::' . $name);
        }

        throw new Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
    }

}