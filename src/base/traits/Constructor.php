<?php


namespace botCore\base\traits;


use Spatie\DataTransferObject\DataTransferObject;

/**
 * Trait Constructor
 * @package app\botCore\traits
 */
trait Constructor
{
    /**
     * Constructor constructor.
     * @param DataTransferObject $dto
     */
    public function __construct(DataTransferObject $dto)
    {
        foreach ($dto->all() as $property => $value){
            $this->$property = $value instanceof DataTransferObject ? null : $value;
        }
    }
}